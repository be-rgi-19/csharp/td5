﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TD5
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_Quitter_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Btn_OK_Click(object sender, RoutedEventArgs e)
        {
            IPHostEntry ihe;
            try
            {
               ihe = Dns.GetHostEntry(tb_Saisie.Text);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }

            lb_res.Items.Add("Machine" + ihe.HostName);
            lb_res.Items.Add("Adresses IP :");
            foreach (IPAddress ip in ihe.AddressList)
                lb_res.Items.Add(ip);
            {
                
            }

        }
    }
}
